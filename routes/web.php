<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Auth::routes();
Route::get('/', function () {
    if (!Auth::check()) {
        return redirect('/login');
    }

    return redirect('/order/new');
});
Route::get('/order/new', 'OrderController@new')->name('newOrder');
Route::post('/order/new', 'OrderController@post')->name('addToCart');
Route::delete('/order/new', 'OrderController@delete')->name('deleteFromCart');
Route::get('/order/history', 'OrderController@index')->name('orderHistory');
