<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    public function products()
    {
        return $this->belongsToMany('App\Product', 'orders_products')
            ->withTimestamps();
    }

    public function getId()
    {
        return $this->id;
    }

    public static function getAll()
    {
        return DB::table('orders as o')
            ->crossJoin('orders_products as op', 'op.order_id', '=', 'o.id')
            ->crossJoin('products as p1', 'p1.id', '=', 'op.product_id')
            ->leftJoin('products as p2', 'p1.parent_id', '=', 'p2.id')
            ->orWhereNull('p1.parent_id')
            ->select('o.id as order_id', 'p2.name as product_name', 'p1.name as prop_name', 'p1.price as product_price')
            ->get();
    }
}
