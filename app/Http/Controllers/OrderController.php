<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Order, Product, User};
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userId = Auth::user()->id;
        abort_if(is_null($userId), 404);
        $orders = User::find($userId)->orders;

        return view('order.history', ['orders' => $orders]);
    }

    public function new()
    {
        $products = Product::getAll();

        return view('order.new', ['products' => $products]);
    }

    public function post(Request $request)
    {
        $userId = Auth::user()->id;
        abort_if(is_null($userId), 404);
        $productId = $request->json()->get('id');
        $product = Product::findOrFail($productId);
        $orderId = $request->json()->get('order_id');
        if (!empty($orderId)) {
            $order = Order::findOrFail($orderId);
        } else {
            $order = new Order();
            $order->user_id = $userId;
        }
        $product->orders()->save($order);

        return response()->json(['status' => 'ok', 'order_id' => $order->getId()]);
    }

    public function delete(Request $request)
    {
        $userId = Auth::user()->id;
        abort_if(is_null($userId), 404);
        $productId = $request->json()->get('id');
        $orderId = $request->json()->get('order_id');
        $product = Product::findOrFail($productId);
        $order = Order::findOrFail($orderId);
        $product->orders()->detach($order);

        return response()->json(['status' => 'ok', 'order_id' => $orderId]);
    }
}
