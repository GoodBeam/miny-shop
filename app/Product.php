<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getParentId()
    {
        return $this->parent_id;
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order', 'orders_products')
            ->withTimestamps();
    }

    public static function getAll()
    {
        return DB::table('products as p1')
            ->leftJoin('products as p2', 'p2.parent_id', '=', 'p1.id')
            ->orWhereNull('p1.parent_id')
            ->select('p1.id', 'p2.id as prop_id', 'p1.name', 'p2.name as prop', 'p1.price', 'p2.price as prop_price')
            ->get();
    }

    public function getParentName()
    {
        return Product::find($this->parent_id);
    }

    public function parent()
    {
        return $this->belongsTo('Product', 'id');
    }

    public function children()
    {
        return $this->hasMany('Product', 'parent_id');
    }
}
