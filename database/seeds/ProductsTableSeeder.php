<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prod1 = Product::create([
            'parent_id' => null,
            'name'    => 'Ручка',
            'price'   => $this->createPrice(),
        ]);
        $prod2 = Product::create([
            'parent_id' => null,
            'name'    => 'Тетрадь',
            'price'   => $this->createPrice(),
        ]);
        Product::create([
            'parent_id' => null,
            'name'    => 'Карандаш',
            'price'   => $this->createPrice(),
        ]);
        Product::create([
            'parent_id' => $prod2->getId(),
            'name'    => 'в клеточку',
            'price'   => $this->createPrice(),
        ]);
        Product::create([
            'parent_id' => $prod2->getId(),
            'name'    => 'в строчку',
            'price'   => $this->createPrice(),
        ]);
        Product::create([
            'parent_id' => $prod1->getId(),
            'name'    => 'синяя паста',
            'price'   => $this->createPrice(),
        ]);
        Product::create([
            'parent_id' => $prod1->getId(),
            'name'    => 'красная паста',
            'price'   => $this->createPrice(),
        ]);
    }

    public function createPrice()
    {
        return round(sqrt(rand()) / 2, 2);
    }
}
