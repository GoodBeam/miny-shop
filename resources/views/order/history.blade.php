@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if($orders->count() === 0)
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title">Вы еще не делали заказов</h5>
                            <a href="{{route('newOrder')}}" class="btn btn-primary">Заказать</a>
                        </div>
                    </div>
                @else
                    @foreach($orders as $order)
                        <div class="col-xs-6">
                            <div class="card">
                                <div class="card-header">заказ № {{$order->id}}</div>
                                <div class="card-body">
                                    <div class="row">
                                        @if($order->products->count() === 0)
                                            <h5 class="card-title">Данный заказ пуст</h5>
                                        @else
                                            @foreach($order->products as $key => $product)
                                                <div class="card" style="width: 18rem;">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{$product->getParentName($product->parent_id)['name']}} {{$product->name}}</h5>
                                                        <p class="card-text">Очень {{$product->name ?? 'крутой товар'}}</p>
                                                        <p class="card-link">${{$product->price}}</p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection