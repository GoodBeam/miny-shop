@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Новый заказ</div>

                    <div class="card-body">
                        <div id="products" class="row list-group">
                            @foreach($products as $key => $product)
                                <div class="item  col-xs-12 col-lg-12 list-group-item">
                                    <div class="thumbnail">
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">{{$product->name . ' ' . $product->prop}}</h4>
                                            <p class="group inner list-group-item-text">
                                                Очень {{$product->prop  ?? 'крутой товар'}}</p>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12">
                                                    <p class="lead">
                                                        ${{ $product->prop_price ?? $product->price }}</p>
                                                </div>
                                                <div class="col-xs-12 col-md-12">
                                                    <a data-id="{{ $product->prop_id ?? $product->id }}"
                                                       class="add-to-cart btn btn-success">Добавить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        let orderId;
        $(function () {
            $('[data-id]').on('click', function (e) {
                e.preventDefault();
                let id = $(this).data('id'),
                    action = 'POST',
                    button = $(this),
                    data = {
                        id: id
                    };
                if (!button.hasClass('btn-success')) {
                    action = 'DELETE';
                }
                if (orderId !== undefined) {
                    data.order_id = orderId;
                }
                $(this).addClass('disabled');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route('addToCart') }}',
                    data: JSON.stringify(data),
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: action,
                    type: action,
                    success: function (b) {
                        if (b.status === 'ok') {
                            if (button.hasClass('btn-success')) {
                                button.removeClass('btn-success');
                                button.addClass('btn-danger');
                                button.text('Удалить');
                            } else {
                                button.removeClass('btn-danger');
                                button.addClass('btn-success');
                                button.text('Добавить');
                                action = 'DELETE';
                            }
                            orderId = b.order_id;
                        }
                    },
                    error: function (e, b, a) {
                        console.log(e, b, a);
                    },
                    complete: function () {
                        button.removeClass('disabled');
                    }
                })
            })
        })
    </script>
@endsection